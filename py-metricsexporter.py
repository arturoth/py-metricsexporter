
#!/usr/bin/env python
# -*- coding: utf-8 -*-

from prometheus_client import start_http_server, Metric, REGISTRY
import json
import requests
import sys
import time

def isCamel(str):
    return str != str.lower() and str != str.upper() and "_" not in str

def strCamelToAbreviature(str):
    count = 1
    for i in range(1, len(str) - 1):
        if (i == 1):
            ab = str[i-1]
            #ab = (str[i], sep=' ', end='', flush=True)
        if (str[i].isupper()):
            count += 1
            ab += str[i]
    return ab

def isDotSplit(str):
    return '.' in str

def strDotSplitToAbreviature(str):
    ab = ''
    dotstr = str.split(".")
    for dot in dotstr:
        if dot[0].isalpha() is True:
            ab += dot[0]
    
    # next version
    #if '/' in str: 
    #    uri = str.split('/')
    #    uname = uri[3].lower() 

    #    print(uname.replace('_', ''))
        
    return ab

class JsonCollector(object):
    def __init__(self, endpoint):
        self._endpoint = endpoint

    def collect(self): #Fetch the JSON
        response = json.loads(requests.get(self._endpoint, auth = ('rest', 'rest.123')).content.decode('UTF-8'))
        # response = json.loads(requests.get(self._endpoint).content.decode('UTF-8'))

        # Metrics with labels for the documents loaded Gauges
        metric = Metric('svc_gauges', 'Gauges', 'summary')
        for k, v in response['gauges'].items():
            # if k != "jvm.thread-states.deadlocks":
            if type(response['gauges'][k]['value']) != type([]):
                metric.add_sample('svc_gauges', value = response['gauges'][k]['value'], labels = {'repository': k })

        yield metric

        sum=0
        svc=0
        svclst = []
        # Metrics with labels for the documents Timers
        for key, val in response['timers'].items():
            #print(key,val)
            # Needed to build svc keys for prometheus
            if (isCamel(key)) and (isDotSplit(key)) is False:
                svcstr = ( (strCamelToAbreviature(key)).lower() + '_' + str(svc) )
            else: 
                svcstr = ( (strDotSplitToAbreviature(key)).lower() + '_' + str(svc) )

            # append to svclist
            svclst.append(svcstr)

            # append into list
            if svcstr in svclst:
                svcln = svcstr
            else:
                # string will be : acm1pt_0
                svcsp = svcstr.split("_")
                newsvc = int( svcsp[1] ) + 1
                # and will be : acm1pt_1
                svcln = ( svcsp[0] + '_' + str(newsvc) )

            
            #pmthead = 'svc_timers_' + str(sum)
            pmthead = 'svc_timers_' + str(svcln)
            metric = Metric(pmthead, key, 'summary')
            for idx, res in response['timers'][key].items():
                if idx == "duration_units":
                    continue
                elif idx == "rate_units":
                    continue
                else:
                    metric.add_sample(pmthead, value = res, labels = {'repository': idx })

            sum=sum+1

            yield metric

if __name__ == '__main__': #Usage: json_exporter.py port endpoint
    start_http_server(int(sys.argv[1]))
    REGISTRY.register(JsonCollector(sys.argv[2]))

    while True: time.sleep(5)